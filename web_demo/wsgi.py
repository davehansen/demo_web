from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from blueprints import root, admin

app = Flask(__name__, instance_relative_config=True)
app.config.from_object('web_demo.config')
app.config.from_pyfile('application.cfg', silent=True)

db = SQLAlchemy(app)

app.register_blueprint(root)
app.register_blueprint(admin, url_prefix='/admin')
