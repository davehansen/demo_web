from web_demo.wsgi import db

class Upload(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(1024))
    user_id = db.Column(db.String(128))
    file_name = db.Column(db.String(32), unique=True)

    def __init__(self, title, user_id, file_name):
        self.title = title
        self.user_id = user_id
        self.file_name = file_name

    def __repr__(self):
        return '<Upload %r>' % self.file_name


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), unique=True)
    password = db.Column(db.String(128))

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return '<User %r>' % self.username
