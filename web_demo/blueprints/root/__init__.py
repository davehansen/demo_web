from flask import Blueprint
from flask import render_template
from flask import request

root = Blueprint(
    'root',
    __name__,
    template_folder='templates',
    static_folder='static'
)


@root.route('/', methods=['POST'])
def index():
    user_name = request.json.get('name')
    return render_template('root/index.html', user_name=user_name)


@root.route('/', methods=['GET'])
def test_index():
    user_name = request.args['name']
    return render_template('root/index.html', user_name=user_name)


@root.route('/favicon.ico')
def static_css():
    return root.send_static_file('favicon.ico')
