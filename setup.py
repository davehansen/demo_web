from setuptools import setup

setup(
    name='WebDemo',
    version='0.0.0',
    install_requires=[
        'flask',
        'Flask-SQLAlchemy'
    ]
)
