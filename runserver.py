if __name__ == '__main__':
    from web_demo.wsgi import app
    from web_demo.wsgi import db

    @app.before_first_request
    def initialize_database():
        from os import path as op

        if not op.isfile(db.engine.url.database):
            import web_demo.models

            db.create_all()

    app.run(debug=True)
