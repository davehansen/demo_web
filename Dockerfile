FROM ubuntu:16.04

# Install.
RUN \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y python python-pip nginx gunicorn supervisor libmysqlclient-dev git

RUN git clone https://davehansen@bitbucket.org/davehansen/demo_web.git /opt/demo_web
COPY demo_web/application.cfg /opt/demo_web/demo_web/application.cfg

RUN pip install -e /opt/demo_web

# Setup nginx
RUN rm /etc/nginx/sites-enabled/default
COPY ./etc/flask.conf /etc/nginx/sites-available/

RUN ln -s /etc/nginx/sites-available/flask.conf /etc/nginx/sites-enabled/flask.conf
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Setup supervisord
RUN mkdir -p /var/log/supervisor
COPY ./etc/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY ./etc/gunicorn.conf /etc/supervisor/conf.d/gunicorn.conf

# Start processes
CMD ["/usr/bin/supervisord"]
