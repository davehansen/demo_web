from flask import Flask
app = Flask(__name__)

OUTPUT = '''<html>

<body>

Hello $variable_passed_from_the_server

</body>

</html>'''

@app.route('/')
def hello_world():
    response_output = OUTPUT
    return response_output


if __name__ == '__main__':
    app.run(debug=True)